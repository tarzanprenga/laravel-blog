<!doctype html>
<html lang="en">
<head>
    @include('partials.header')
    <title>Dashboard</title>
</head>

<body>

<div class="wrapper">

    @include('partials.sidebar')

    <div class="main-panel">

        @include('partials.nav')

        <div class="content">

            <div class="container-fluid">

                @yield('content');

            </div>

        </div>

        @include('partials.footer')

    </div>
</div>

</body>

@include('partials.scripts')

</html>
