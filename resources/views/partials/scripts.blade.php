<!--   Core JS Files   -->
<script src="{{ URL::to('/js/jquery-3.1.0.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('/js/material.min.js') }}" type="text/javascript"></script>


<!--  Notifications Plugin    -->
<script src="{{ URL::to('/js/bootstrap-notify.js') }}"></script>

<!-- Material Dashboard javascript methods -->
<script src="{{ URL::to('/js/material-dashboard.js') }}"></script>

<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ URL::to('/js/demo.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>